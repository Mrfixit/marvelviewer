package com.dbel.marvelviewer

import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import com.dbel.marvelviewer.domain.repo.ComicsRepository
import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCase
import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCaseImpl
import com.dbel.marvelviewer.utils.schedulers.SchedulerManager
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class Get100ComicsUseCaseTest {

    @Mock
    lateinit var repo: ComicsRepository

    @Mock
    lateinit var schedulerManager: SchedulerManager

    lateinit var useCase: Get100ComicsUseCase

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        useCase = Get100ComicsUseCaseImpl(schedulerManager, repo)
    }

    @Test
    fun useCase_shouldCallRepository_and_shouldReturn_100_items() {
        //Given
        val rightResult = mutableListOf<ComicsLo>()

        for (i in 0..99) {
            rightResult.add(ComicsLo(i, "test", "test"))
        }

        whenever(schedulerManager.getIoScheduler()).thenReturn(Schedulers.trampoline())
        whenever(schedulerManager.getMainScheduler()).thenReturn(Schedulers.trampoline())

        whenever(repo.getComics(any(), any())).thenReturn(Flowable.just(rightResult))

        //WHEN
        val testObserver = useCase.execute().test()
        testObserver.awaitTerminalEvent()

        //THEN

        verify(repo, times(1)).getComics(any(), any())

        testObserver.assertNoErrors()
            .assertValueCount(1)
            .assertValue { it.size == 100 }

    }


}