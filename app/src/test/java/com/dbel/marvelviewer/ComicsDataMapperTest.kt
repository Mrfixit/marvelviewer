package com.dbel.marvelviewer

import com.dbel.marvelviewer.data.models.comics.ComicsDto
import com.dbel.marvelviewer.data.models.comics.ThumbnailDto
import com.dbel.marvelviewer.data.models.comics.toDomainComicsList
import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import org.junit.Before
import org.junit.Test

class ComicsDataMapperTest {

    @Before
    fun setUp() {
    }

    @Test
    fun dataMapper_dto_to_domain_convert() {

        val realResult = mutableListOf<ComicsLo>(
            ComicsLo(1, "test", "test.jpg"),
            ComicsLo(2, "test", "test.jpg"),
            ComicsLo(3, "test", "test.jpg"),
            ComicsLo(4, "test", "test.jpg")
        )

        val itemsDto = mutableListOf<ComicsDto>(
            ComicsDto(1, "test", ThumbnailDto("test", "jpg")),
            ComicsDto(2, "test", ThumbnailDto("test", "jpg")),
            ComicsDto(3, "test", ThumbnailDto("test", "jpg")),
            ComicsDto(4, "test", ThumbnailDto("test", "jpg"))
        )

        //WHEN
        val isEqual = realResult == itemsDto.toDomainComicsList()

        //THEN
        assert(isEqual)
    }


}