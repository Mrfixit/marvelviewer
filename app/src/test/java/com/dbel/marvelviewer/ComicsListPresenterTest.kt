package com.dbel.marvelviewer

import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCase
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.toViewComicsList
import com.dbel.marvelviewer.presentation.comics.mvp.presenters.ComicsListPresenter
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsListView
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ComicsListPresenterTest {

    @Mock
    lateinit var view: ComicsListView

    @Mock
    lateinit var get100ComicsUseCase: Get100ComicsUseCase

    private lateinit var presenter: ComicsListPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = ComicsListPresenter(get100ComicsUseCase)
        presenter.attachView(view)
    }

    @Test
    fun firstInit_shouldCallUseCase_andReturnData_andShowIt() {
        //GIVEN
        val resultComicsList: List<ComicsLo> = arrayListOf(ComicsLo(1, "test", "test"))
        whenever(get100ComicsUseCase.execute()).thenReturn(Flowable.just(resultComicsList))

        //WHEN
        presenter.init()

        //THEN

        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideContent()
        verify(view, times(1)).showContent()
        verify(view, times(1)).hideLoading()
        verify(view, times(1)).setComics(
            argWhere { it == resultComicsList.toViewComicsList() }
        )

    }

    @Test
    fun loadData_shouldCallUseCaseWithError_andReturnError_andShowIt() {
        //GIVEN
        whenever(get100ComicsUseCase.execute()).thenReturn(Flowable.error(NoSuchMethodError()))

        //WHEN
        presenter.loadData()

        //THEN
        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideContent()
        verify(view, times(1)).showContent()
        verify(view, times(1)).hideLoading()
        verify(view, times(1)).showError(any())

    }

    @Test
    fun refreshData_shouldCallUseCase_andReturnData_andShowIt() {
        //GIVEN
        val resultComicsList: List<ComicsLo> = arrayListOf(ComicsLo(1, "test", "test"))
        whenever(get100ComicsUseCase.execute()).thenReturn(Flowable.just(resultComicsList))

        //WHEN
        presenter.refreshData()

        //THEN
        verify(view, times(1)).showRefreshLoading()
        verify(view, times(1)).hideRefreshLoading()
        verify(view, times(1)).setComics(
            argWhere { it == resultComicsList.toViewComicsList() }
        )

    }


}