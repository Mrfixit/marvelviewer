package com.dbel.marvelviewer

import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.Comics
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.toViewComicsList
import org.junit.Before
import org.junit.Test

class ComicsViewMapperTest {

    @Before
    fun setUp() {
    }

    @Test
    fun dataMapper_dto_to_domain_convert() {
        //Given

        val expectedResult = mutableListOf<Comics>(
            Comics(1, "test", "test.jpg"),
            Comics(2, "test", "test.jpg"),
            Comics(3, "test", "test.jpg"),
            Comics(4, "test", "test.jpg")
        )

        val itemsLo = mutableListOf<ComicsLo>(
            ComicsLo(1, "test", "test.jpg"),
            ComicsLo(2, "test", "test.jpg"),
            ComicsLo(3, "test", "test.jpg"),
            ComicsLo(4, "test", "test.jpg")
        )


        //WHEN
        val isEqual = expectedResult == itemsLo.toViewComicsList()

        //THEN
        assert(isEqual)

    }


}