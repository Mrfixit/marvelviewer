package com.dbel.marvelviewer.utils.app

import android.app.Application
import android.content.Context
import com.dbel.marvelviewer.utils.schedulers.SchedulerManager
import com.dbel.marvelviewer.utils.schedulers.SchedulerManagerImpl
import toothpick.config.Module

//Application level Module(aka application scope)
class ComicsApplicationModule(application: Application) : Module() {

    init {
        bind(Application::class.java).toInstance(application)
        bind(Context::class.java).toInstance(application)
        bind(SchedulerManager::class.java).toInstance(SchedulerManagerImpl())
    }

}