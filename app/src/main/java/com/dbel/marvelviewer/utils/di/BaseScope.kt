package com.dbel.marvelviewer.utils.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class BaseScope