package com.dbel.marvelviewer.utils.app

import android.app.Application
import com.dbel.marvelviewer.utils.di.Injection
import com.jakewharton.threetenabp.AndroidThreeTen

class ComicsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Injection.init(this)

        setupTime()
    }


    private fun setupTime() {
        AndroidThreeTen.init(this)
    }


}