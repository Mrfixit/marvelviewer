package com.dbel.marvelviewer.domain.usecases

import com.dbel.marvelviewer.domain.core.BaseUseCase
import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import com.dbel.marvelviewer.domain.repo.ComicsRepository
import com.dbel.marvelviewer.utils.schedulers.SchedulerManager
import io.reactivex.Flowable

class Get100ComicsUseCaseImpl(
    schedulerManager: SchedulerManager,
    private val repo: ComicsRepository
) : Get100ComicsUseCase, BaseUseCase(schedulerManager) {
    override fun execute(): Flowable<List<ComicsLo>> {
        return repo.getComics(COUNT, 0)
            .applySchedulers()
    }

    companion object {
        private const val COUNT = 100
    }
}