package com.dbel.marvelviewer.domain.repo

import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import io.reactivex.Flowable

interface ComicsRepository {
    fun getComics(limit: Int, offset: Int): Flowable<List<ComicsLo>>
}