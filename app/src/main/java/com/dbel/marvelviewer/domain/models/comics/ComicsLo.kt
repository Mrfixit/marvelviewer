package com.dbel.marvelviewer.domain.models.comics

import com.dbel.marvelviewer.domain.models.BaseLo

data class ComicsLo(
    val id: Int,
    val title: String,
    val imageUrl: String
) : BaseLo()