package com.dbel.marvelviewer.domain.core.di.providers

import com.dbel.marvelviewer.domain.repo.ComicsRepository
import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCase
import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCaseImpl
import com.dbel.marvelviewer.utils.schedulers.SchedulerManager
import javax.inject.Inject
import javax.inject.Provider

class Get100ComicsUseCaseProvider @Inject constructor(
    private val schedulerManager: SchedulerManager,
    private val repo: ComicsRepository
) : Provider<Get100ComicsUseCase> {
    override fun get(): Get100ComicsUseCase {
        return Get100ComicsUseCaseImpl(schedulerManager, repo)
    }
}