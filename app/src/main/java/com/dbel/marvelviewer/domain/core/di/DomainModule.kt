package com.dbel.marvelviewer.domain.core.di

import com.dbel.marvelviewer.domain.core.di.providers.Get100ComicsUseCaseProvider
import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCase
import toothpick.config.Module

class DomainModule() : Module() {
    init {
//        bind(UseCase::class.java).toProvider(UseCaseProvider::class.java)
        bind(Get100ComicsUseCase::class.java).toProvider(Get100ComicsUseCaseProvider::class.java)
            .providesSingletonInScope()
    }
}