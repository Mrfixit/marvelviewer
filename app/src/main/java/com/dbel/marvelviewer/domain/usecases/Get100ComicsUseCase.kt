package com.dbel.marvelviewer.domain.usecases

import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import io.reactivex.Flowable

interface Get100ComicsUseCase {
    fun execute(): Flowable<List<ComicsLo>>
}