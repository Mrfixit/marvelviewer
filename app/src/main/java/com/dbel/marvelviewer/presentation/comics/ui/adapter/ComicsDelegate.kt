package com.dbel.marvelviewer.presentation.comics.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dbel.marvelviewer.R
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.Comics
import com.dbel.marvelviewer.presentation.core.ui.adapter.AdapterItem
import com.dbel.marvelviewer.presentation.core.ui.adapter.AdapterViewHolder
import com.dbel.marvelviewer.utils.inflate
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.delegate_comics.view.*

class ComicsDelegate : AdapterDelegate<List<AdapterItem>>() {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = parent.inflate(R.layout.delegate_comics)
        return ViewHolder(view)
    }

    override fun isForViewType(items: List<AdapterItem>, position: Int): Boolean {
        return items[position] is Comics
    }

    override fun onBindViewHolder(
        items: List<AdapterItem>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        holder as ViewHolder
        holder.bind(items[position] as Comics)
    }


    companion object {
        private class ViewHolder(view: View) : AdapterViewHolder<Comics>(view) {
            override fun bind(item: Comics, position: Int) {
                Picasso.get()
                    .load(item.imageUrl)
                    .fit()
                    .centerInside()
                    .into(itemView.comics_image)

                val nameYearString = item.title + "(${item.year})"
                itemView.comics_title.text = nameYearString
            }

        }
    }

}