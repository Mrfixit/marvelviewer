package com.dbel.marvelviewer.presentation.comics.mvp.presenters

import com.dbel.marvelviewer.presentation.comics.di.scopes.ComicsScope
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsView
import com.dbel.marvelviewer.presentation.comics.ui.navigation.ComicsNavigator
import com.dbel.marvelviewer.presentation.core.mvp.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@ComicsScope
class ComicsPresenter @Inject constructor(
    private val router: Router
) : BasePresenter<ComicsView>() {

    fun init() {
        router.newRootScreen(ComicsNavigator.Screens.ComicsListScreen())
    }

    fun onBackPress() {
        router.exit()
    }

}