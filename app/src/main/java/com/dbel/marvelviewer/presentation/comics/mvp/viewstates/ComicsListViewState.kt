package com.dbel.marvelviewer.presentation.comics.mvp.viewstates

import android.os.Bundle
import com.dbel.marvelviewer.presentation.comics.di.scopes.ComicsListScope
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.Comics
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsListView
import com.dbel.marvelviewer.presentation.core.mvp.BaseViewState
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState
import javax.inject.Inject

@ComicsListScope
class ComicsListViewState @Inject constructor() : BaseViewState<ComicsListView>() {

    private var items: ArrayList<Comics> = arrayListOf()

    private var state: State = State.LOADING

    override fun saveInstanceState(out: Bundle) {
        out.putParcelableArrayList(KEY_ITEMS, items)
        out.putSerializable(KEY_STATE, state)
    }

    override fun apply(view: ComicsListView?, retained: Boolean) {
        when (state) {
            State.LOADING -> view?.onNewViewStateInstance()
            State.CONTENT -> {
                view?.setComics(items)
                view?.hideLoading()
                view?.hideRefreshLoading()
                view?.showContent()
            }
        }
    }

    override fun restoreInstanceState(input: Bundle?): RestorableViewState<ComicsListView> {
        input?.let {
            state = it.getSerializable(KEY_STATE) as State
            this.items = it.getParcelableArrayList<Comics>(KEY_ITEMS) as ArrayList<Comics>
        }
        return this
    }

    fun setLoadingState() {
        state = State.LOADING
    }

    fun setContentState(items: List<Comics>) {
        state = State.CONTENT
        this.items = ArrayList(items)
    }

    companion object {
        private enum class State {
            LOADING,
            CONTENT
        }

        private const val KEY_STATE = "KEY_STATE"
        private const val KEY_ITEMS = "KEY_ITEMS"
    }
}