package com.dbel.marvelviewer.presentation.comics.di.modules

import com.dbel.marvelviewer.presentation.comics.ui.fragments.ComicsListFragment
import toothpick.config.Module

class ComicsListModule(fragment: ComicsListFragment) : Module() {
    init {
        bind(ComicsListFragment::class.java).toInstance(fragment)
    }
}