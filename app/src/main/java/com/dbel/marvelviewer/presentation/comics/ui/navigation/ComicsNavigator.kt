package com.dbel.marvelviewer.presentation.comics.ui.navigation

import androidx.fragment.app.Fragment
import com.dbel.marvelviewer.presentation.comics.ui.activities.ComicsActivity
import com.dbel.marvelviewer.presentation.comics.ui.fragments.ComicsListFragment
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.android.support.SupportAppScreen

class ComicsNavigator(activity: ComicsActivity) :
    SupportAppNavigator(activity, activity.getContainerId()) {

    override fun createFragment(screen: SupportAppScreen?): Fragment {
        //animation here
        return super.createFragment(screen)
    }

    internal object Screens {

        class ComicsListScreen : SupportAppScreen() {
            override fun getFragment(): Fragment {
                return ComicsListFragment.newInstance()
            }
        }


    }
}