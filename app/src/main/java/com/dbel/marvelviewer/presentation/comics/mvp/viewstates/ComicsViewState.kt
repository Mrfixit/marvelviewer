package com.dbel.marvelviewer.presentation.comics.mvp.viewstates

import android.os.Bundle
import com.dbel.marvelviewer.presentation.comics.di.scopes.ComicsScope
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsView
import com.dbel.marvelviewer.presentation.core.mvp.BaseViewState
import com.hannesdorfmann.mosby3.mvp.viewstate.RestorableViewState
import javax.inject.Inject

@ComicsScope
class ComicsViewState @Inject constructor() : BaseViewState<ComicsView>() {

    override fun saveInstanceState(out: Bundle) {
    }

    override fun apply(view: ComicsView?, retained: Boolean) {
    }

    override fun restoreInstanceState(`in`: Bundle?): RestorableViewState<ComicsView> {
        return this
    }

}