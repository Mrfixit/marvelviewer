package com.dbel.marvelviewer.presentation.comics.di.scopes

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ComicsScope