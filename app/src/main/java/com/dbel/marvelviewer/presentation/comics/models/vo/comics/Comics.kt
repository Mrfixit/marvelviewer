package com.dbel.marvelviewer.presentation.comics.models.vo.comics

import com.dbel.marvelviewer.presentation.comics.models.items.BaseComicsAdapterItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Comics(
    val id: Int,
    val title: String,
    val year: Int,
    val imageUrl: String
) : BaseComicsAdapterItem()