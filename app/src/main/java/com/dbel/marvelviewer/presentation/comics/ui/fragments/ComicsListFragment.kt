package com.dbel.marvelviewer.presentation.comics.ui.fragments

import android.os.Bundle
import android.view.View
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.dbel.marvelviewer.R
import com.dbel.marvelviewer.presentation.comics.di.modules.ComicsListModule
import com.dbel.marvelviewer.presentation.comics.di.scopes.ComicsListScope
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.Comics
import com.dbel.marvelviewer.presentation.comics.mvp.presenters.ComicsListPresenter
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsListView
import com.dbel.marvelviewer.presentation.comics.mvp.viewstates.ComicsListViewState
import com.dbel.marvelviewer.presentation.comics.ui.adapter.ComicsDelegate
import com.dbel.marvelviewer.presentation.core.ui.BaseActivity
import com.dbel.marvelviewer.presentation.core.ui.BaseFragment
import com.dbel.marvelviewer.presentation.core.ui.OnBaseCallbackListener
import com.dbel.marvelviewer.presentation.core.ui.adapter.SyncAdapter
import com.dbel.marvelviewer.presentation.core.ui.adapter.VerticalLinearSpaceItemDecoration
import kotlinx.android.synthetic.main.fragment_comics_list.*
import toothpick.config.Module
import javax.inject.Inject

class ComicsListFragment : BaseFragment<OnBaseCallbackListener,
        ComicsListView,
        ComicsListPresenter,
        ComicsListViewState>(), ComicsListView {

    @Inject
    protected lateinit var injectedPresenter: ComicsListPresenter

    @Inject
    protected lateinit var injectedViewState: ComicsListViewState

    private lateinit var adapter: SyncAdapter

    override fun getDeclaredModules(): Array<Module> = arrayOf(ComicsListModule(this))

    override fun getDeclaredScope(): Class<*> = ComicsListScope::class.java

    override fun getLayout(): Int = R.layout.fragment_comics_list

    override fun createPresenter(): ComicsListPresenter = injectedPresenter

    override fun createViewState(): ComicsListViewState = injectedViewState

    override fun onNewViewStateInstance() {
        getPresenter().init()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    override fun setComics(items: List<Comics>) {
        adapter.setItems(items)
        getViewState()?.setContentState(items)
    }

    override fun showContent() {
        swipe_comics.visibility = View.VISIBLE
    }

    override fun hideContent() {
        swipe_comics.visibility = View.INVISIBLE
    }

    override fun showLoading() {
        progress_comics.visibility = View.VISIBLE
        getViewState()?.setLoadingState()
    }

    override fun hideLoading() {
        progress_comics.visibility = View.INVISIBLE
    }

    override fun showRefreshLoading() {
        swipe_comics.isRefreshing = true
        getViewState()?.setLoadingState()
    }

    override fun hideRefreshLoading() {
        swipe_comics.isRefreshing = false
        getViewState()?.setLoadingState()
    }

    private fun initUi() {
        toolbar_comics.setTitle(R.string.app_name)
        (activity as BaseActivity<*, *, *>).setSupportActionBar(toolbar_comics)

        val marginItems = resources.getDimensionPixelOffset(R.dimen.margin_items)
        val marginTop = resources.getDimensionPixelOffset(R.dimen.margin_comics_list_top)
        val marginBottom = resources.getDimensionPixelOffset(R.dimen.margin_comics_list_bottom)

        adapter = SyncAdapter()

        adapter.addDelegates(
            arrayOf(
                ComicsDelegate()
            )
        )

        recycler_comics.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        recycler_comics.adapter = adapter

        recycler_comics.addItemDecoration(
            VerticalLinearSpaceItemDecoration(
                marginItems,
                marginTop,
                marginBottom
            )
        )

        swipe_comics.setOnRefreshListener { getPresenter().refreshData() }


        edittext_name_comics.addTextChangedListener { getPresenter().onComicsNameFilterChange(it.toString()) }
        edittext_year_comics.addTextChangedListener { getPresenter().onComicsYearFilterChange(it.toString()) }
    }


    companion object {
        fun newInstance(): Fragment {
            return ComicsListFragment()
        }
    }

}