package com.dbel.marvelviewer.presentation.comics.models.vo.comics

import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import kotlin.random.Random


fun ComicsLo.toViewObject(): Comics =
    Comics(
        this.id,
        this.title,
        Random.nextInt(1900, 2019),
        this.imageUrl
    )

fun List<ComicsLo>.toViewComicsList(): List<Comics> =
    this.map { it.toViewObject() }.toList()


