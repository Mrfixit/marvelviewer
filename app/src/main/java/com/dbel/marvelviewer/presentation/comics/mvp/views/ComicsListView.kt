package com.dbel.marvelviewer.presentation.comics.mvp.views

import com.dbel.marvelviewer.presentation.comics.models.vo.comics.Comics
import com.dbel.marvelviewer.presentation.core.mvp.BaseView

interface ComicsListView : BaseView {
    fun setComics(items: List<Comics>)
    fun showContent()
    fun hideContent()
    fun showRefreshLoading()
    fun hideRefreshLoading()
    fun onNewViewStateInstance()
}