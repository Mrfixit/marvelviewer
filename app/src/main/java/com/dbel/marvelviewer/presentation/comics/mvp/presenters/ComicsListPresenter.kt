package com.dbel.marvelviewer.presentation.comics.mvp.presenters

import com.dbel.marvelviewer.domain.usecases.Get100ComicsUseCase
import com.dbel.marvelviewer.presentation.comics.di.scopes.ComicsListScope
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.Comics
import com.dbel.marvelviewer.presentation.comics.models.vo.comics.toViewComicsList
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsListView
import com.dbel.marvelviewer.presentation.core.mvp.BasePresenter
import javax.inject.Inject

@ComicsListScope
class ComicsListPresenter @Inject constructor(
    private val get100ComicsUseCase: Get100ComicsUseCase
) : BasePresenter<ComicsListView>() {

    private var comicsNameFilter: String = ""
    private var comicsYearFilter: String = ""

    private var comicsList = listOf<Comics>()

    fun init() {
        loadData()
    }

    fun loadData() {
        add(get100ComicsUseCase.execute()
            .doOnSubscribe {
                ifViewAttached { view ->
                    view.hideContent()
                    view.showLoading()
                }
            }
            .doOnComplete {
                ifViewAttached { view ->
                    view.showContent()
                    view.hideLoading()
                }
            }
            .doOnError {
                ifViewAttached { view ->
                    view.hideLoading()
                    view.showContent()
                }
            }
            .map { it.toViewComicsList() }
            .subscribe({
                this.comicsList = ArrayList(it)
                invalidateData()
            }, {
                ifViewAttached { view -> view.showError("Something went wrong") }
            })
        )
    }

    fun refreshData() {
        add(get100ComicsUseCase.execute()
            .doOnSubscribe {
                ifViewAttached { view -> view.showRefreshLoading() }
            }
            .doOnComplete {
                ifViewAttached { view -> view.hideRefreshLoading() }
            }
            .doOnError {
                ifViewAttached { view -> view.hideRefreshLoading() }
            }
            .map { it.toViewComicsList() }
            .subscribe({
                this.comicsList = ArrayList(it)
                invalidateData()
            }, {
                ifViewAttached { view -> view.showError("Something went wrong") }
            })
        )
    }

    fun onComicsNameFilterChange(comicsNameFilter: String) {
        if (this.comicsNameFilter != comicsNameFilter) {
            this.comicsNameFilter = comicsNameFilter
            invalidateData()
        }
    }


    fun onComicsYearFilterChange(comicsYearFilter: String) {
        if (this.comicsYearFilter != comicsYearFilter) {
            this.comicsYearFilter = comicsYearFilter
            invalidateData()
        }
    }

    private fun invalidateData() {
        if (comicsList.isNotEmpty()) {

            if (comicsNameFilter.isNotEmpty()) {
                comicsList = comicsList.filter { it.title.startsWith(comicsNameFilter) }
            }

            if (comicsYearFilter.isNotEmpty()) {
                comicsList = comicsList.filter { it.year >= comicsYearFilter.toInt() }
            }

            ifViewAttached { view -> view.setComics(comicsList) }
        }
    }

}