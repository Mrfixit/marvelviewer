package com.dbel.marvelviewer.presentation.comics.di.modules

import com.dbel.marvelviewer.presentation.comics.ui.activities.ComicsActivity
import com.dbel.marvelviewer.presentation.comics.ui.navigation.ComicsNavigator
import com.dbel.marvelviewer.presentation.core.ui.navigation.di.BaseNavigatorObjects
import com.dbel.marvelviewer.presentation.core.ui.navigation.di.RouterProvider
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class ComicsModule(activity: ComicsActivity) : Module() {

    init {
        bind(ComicsActivity::class.java).toInstance(activity)
        bind(ComicsNavigator::class.java).toInstance(ComicsNavigator(activity))
        bind(String::class.java).withName(BaseNavigatorObjects.ROUTER_KEY)
            .toInstance(activity.getSimpleName())
        bind(Router::class.java).toProvider(RouterProvider::class.java).providesSingletonInScope()
    }

}