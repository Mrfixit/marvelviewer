package com.dbel.marvelviewer.presentation.core.ui.adapter

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

abstract class AdapterItem(
    private val privateId: String = UUID.randomUUID().toString()
) : Parcelable