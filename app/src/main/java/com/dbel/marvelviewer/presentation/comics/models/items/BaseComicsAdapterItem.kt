package com.dbel.marvelviewer.presentation.comics.models.items

import com.dbel.marvelviewer.presentation.core.ui.adapter.AdapterItem

abstract class BaseComicsAdapterItem : AdapterItem()