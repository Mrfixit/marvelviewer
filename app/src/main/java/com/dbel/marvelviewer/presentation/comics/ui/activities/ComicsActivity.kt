package com.dbel.marvelviewer.presentation.comics.ui.activities

import android.content.Context
import android.content.Intent
import com.dbel.marvelviewer.R
import com.dbel.marvelviewer.presentation.comics.di.modules.ComicsModule
import com.dbel.marvelviewer.presentation.comics.di.scopes.ComicsScope
import com.dbel.marvelviewer.presentation.comics.mvp.presenters.ComicsPresenter
import com.dbel.marvelviewer.presentation.comics.mvp.views.ComicsView
import com.dbel.marvelviewer.presentation.comics.mvp.viewstates.ComicsViewState
import com.dbel.marvelviewer.presentation.comics.ui.navigation.ComicsNavigator
import com.dbel.marvelviewer.presentation.core.ui.navigation.BaseNavigationActivity
import ru.terrakok.cicerone.Navigator
import toothpick.config.Module
import javax.inject.Inject

class ComicsActivity : BaseNavigationActivity<ComicsView, ComicsPresenter, ComicsViewState>(),
    ComicsView {

    @Inject
    protected lateinit var injectedPresenter: ComicsPresenter

    @Inject
    protected lateinit var injectedViewState: ComicsViewState

    @Inject
    protected lateinit var injectedNavigator: ComicsNavigator

    override fun getContainerId(): Int = R.id.container

    override fun getLocalNavigator(): Navigator = injectedNavigator

    override fun getDeclaredModules(): Array<Module> = arrayOf(ComicsModule(this))

    override fun getDeclaredScope(): Class<*> = ComicsScope::class.java

    override fun getLayout(): Int = R.layout.activity_comics

    override fun onNewViewStateInstance() {
        getPresenter().init()
    }

    override fun createPresenter(): ComicsPresenter = injectedPresenter

    override fun createViewState(): ComicsViewState = injectedViewState


    companion object {

        private fun newIntent(context: Context): Intent {
            return Intent(context, ComicsActivity::class.java)
        }

    }

}