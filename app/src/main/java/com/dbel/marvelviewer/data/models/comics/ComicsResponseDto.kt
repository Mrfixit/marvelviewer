package com.dbel.marvelviewer.data.models.comics

import com.dbel.marvelviewer.data.models.BaseDto
import com.squareup.moshi.Json

data class ComicsResponseDto(
    @Json(name = "code")
    val code: Int,
    @Json(name = "data")
    val data: ComicsResponseDataDto?
) : BaseDto() {

}