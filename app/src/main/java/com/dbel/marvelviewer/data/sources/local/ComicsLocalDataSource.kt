package com.dbel.marvelviewer.data.sources.local

import com.dbel.marvelviewer.data.models.comics.ComicsDto
import com.dbel.marvelviewer.data.sources.ComicsDataSource
import io.reactivex.Flowable
import io.reactivex.Single

class ComicsLocalDataSource : ComicsDataSource {

    override fun loadComics(limit: Int, offset: Int): Flowable<List<ComicsDto>> {
        throw NoSuchMethodError()
    }

    override fun cacheComics(items: List<ComicsDto>): Flowable<List<ComicsDto>> {
        throw NoSuchMethodError()
    }

    override fun isCachedComicsExists(): Single<Boolean> {
        throw NoSuchMethodError()
    }

}