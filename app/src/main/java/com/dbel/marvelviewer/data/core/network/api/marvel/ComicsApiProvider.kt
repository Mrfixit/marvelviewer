package com.dbel.marvelviewer.data.core.network.api.marvel

import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Provider


class ComicsApiProvider @Inject constructor(
    private val retrofit: Retrofit
) : Provider<ComicsApi> {

    override fun get(): ComicsApi {
        return retrofit.create(ComicsApi::class.java)
    }

}