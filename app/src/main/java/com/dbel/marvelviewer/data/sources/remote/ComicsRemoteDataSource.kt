package com.dbel.marvelviewer.data.sources.remote

import android.content.Context
import com.dbel.marvelviewer.R
import com.dbel.marvelviewer.data.core.network.api.marvel.ComicsApi
import com.dbel.marvelviewer.data.models.comics.ComicsDto
import com.dbel.marvelviewer.data.sources.ComicsDataSource
import com.dbel.marvelviewer.utils.md5
import io.reactivex.Flowable
import io.reactivex.Single
import org.threeten.bp.Instant
import java.util.zip.DataFormatException

class ComicsRemoteDataSource(
    private val context: Context,
    private val api: ComicsApi
) : ComicsDataSource {

    override fun loadComics(limit: Int, offset: Int): Flowable<List<ComicsDto>> {
        val params = mutableMapOf<String, String>()
        params.putAll(getAuthParams())
        params[PARAM_LIMIT] = limit.toString()
        params[PARAM_OFFSET] = offset.toString()
        return api.getComics(params)
            .flatMap {
                if (it.code == 200 && it.data != null) {
                    Flowable.just(it.data)
                } else {
                    Flowable.error(DataFormatException())
                }
            }
            .flatMap {
                if (it.results != null) {
                    Flowable.just(it.results)
                } else {
                    Flowable.error(DataFormatException())
                }
            }
    }

    override fun cacheComics(items: List<ComicsDto>): Flowable<List<ComicsDto>> {
        throw NoSuchMethodError()
    }

    override fun isCachedComicsExists(): Single<Boolean> {
        throw NoSuchMethodError()
    }


    private fun getAuthParams(): Map<String, String> {
        val publicKey = context.getString(R.string.public_key)
        val timestamp = Instant.now().toEpochMilli().toString()
        val hash = (timestamp + context.getString(R.string.private_key) + publicKey).md5()

        return mapOf(
            Pair(PARAM_APIKEY, publicKey),
            Pair(PARAM_TIMESTAMP, timestamp),
            Pair(PARAM_HASH, hash)
        )
    }


    companion object {
        private const val PARAM_APIKEY = "apikey"
        private const val PARAM_TIMESTAMP = "ts"
        private const val PARAM_HASH = "hash"
        private const val PARAM_LIMIT = "limit"
        private const val PARAM_OFFSET = "offset"
    }
}