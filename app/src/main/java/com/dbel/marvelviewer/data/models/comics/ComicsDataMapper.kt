package com.dbel.marvelviewer.data.models.comics

import com.dbel.marvelviewer.domain.models.comics.ComicsLo

fun ComicsDto.toDomain(): ComicsLo =
    ComicsLo(
        this.id,
        this.title,
        imageUrl = if (thumbnail == null) {
            val empty = ThumbnailDto.empty()
            empty.path + "." + empty.extension
        } else {
            this.thumbnail.path + "." + this.thumbnail.extension
        }
    )

fun List<ComicsDto>.toDomainComicsList(): List<ComicsLo> =
    this.map { it.toDomain() }.toList()

