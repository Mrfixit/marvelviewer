package com.dbel.marvelviewer.data.models.comics

import com.dbel.marvelviewer.data.models.BaseDto
import com.squareup.moshi.Json

class ThumbnailDto(
    @Json(name = "path")
    val path: String,
    @Json(name = "extension")
    val extension: String
) : BaseDto() {
    companion object {
        fun empty(): ThumbnailDto = ThumbnailDto(
            "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available",
            "jpg"
        )
    }

}