package com.dbel.marvelviewer.data.models.comics

import com.dbel.marvelviewer.data.models.BaseDto
import com.squareup.moshi.Json

data class ComicsResponseDataDto(
    @Json(name = "offset")
    val offset: Int,
    @Json(name = "limit")
    val limit: Int,
    @Json(name = "total")
    val total: Int,
    @Json(name = "results")
    val results: List<ComicsDto>?
) : BaseDto()