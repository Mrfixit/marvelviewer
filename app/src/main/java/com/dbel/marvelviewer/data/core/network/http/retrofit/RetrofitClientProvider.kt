package com.dbel.marvelviewer.data.core.network.http.retrofit

import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class RetrofitClientProvider @Inject constructor(
    val moshi: Moshi
) : Provider<Retrofit> {

    override fun get(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BASE_URL)
            .build()
    }

    private companion object {
        private const val BASE_URL = "https://gateway.marvel.com:443"
    }

}