package com.dbel.marvelviewer.data.sources

import com.dbel.marvelviewer.data.models.comics.ComicsDto
import io.reactivex.Flowable
import io.reactivex.Single

interface ComicsDataSource {
    fun loadComics(limit: Int, offset: Int): Flowable<List<ComicsDto>>
    fun cacheComics(items: List<ComicsDto>): Flowable<List<ComicsDto>>
    fun isCachedComicsExists(): Single<Boolean>
}