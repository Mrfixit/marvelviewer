package com.dbel.marvelviewer.data.core.network.api.marvel

import com.dbel.marvelviewer.data.models.comics.ComicsResponseDto
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ComicsApi {

    @GET("/v1/public/comics")
    fun getComics(@QueryMap params: Map<String, String>): Flowable<ComicsResponseDto>
//d4571e05e7fadd8ec19a2eafbdff80fa
    //https://gateway.marvel.com:443/v1/public/comics?apikey=d4571e05e7fadd8ec19a2eafbdff80fa&hash=92a96489b966a55dd888eaec18dc14a7&ts=1572996861
//    http://i.annihil.us/u/prod/marvel/i/mg/5/90/4c4e014aa3086 + jpg
}