package com.dbel.marvelviewer.data.models.comics

import com.dbel.marvelviewer.data.models.BaseDto
import com.squareup.moshi.Json

data class ComicsDto(
    @Json(name = "id")
    val id: Int,
    @Json(name = "title")
    val title: String,
    @Json(name = "thumbnail")
    val thumbnail: ThumbnailDto?
) : BaseDto()