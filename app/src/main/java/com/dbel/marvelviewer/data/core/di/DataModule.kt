package com.dbel.marvelviewer.data.core.di

import android.app.Application
import com.dbel.marvelviewer.data.core.di.providers.ComicsRepositoryProvider
import com.dbel.marvelviewer.data.core.network.api.marvel.ComicsApi
import com.dbel.marvelviewer.data.core.network.api.marvel.ComicsApiProvider
import com.dbel.marvelviewer.data.core.network.http.moshi.MoshiProvider
import com.dbel.marvelviewer.data.core.network.http.ok.OkHttpClientProvider
import com.dbel.marvelviewer.data.core.network.http.retrofit.RetrofitClientProvider
import com.dbel.marvelviewer.domain.repo.ComicsRepository
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import toothpick.config.Module

class DataModule(application: Application) : Module() {
    init {
        bindRemote(application)
        bindApi()
        bindRepos()
    }


    private fun bindRemote(application: Application) {
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java)
            .providesSingletonInScope()

        bind(Moshi::class.java).toProvider(MoshiProvider::class.java).providesSingletonInScope()

        bind(Retrofit::class.java).toProvider(RetrofitClientProvider::class.java)
            .providesSingletonInScope()

    }

    private fun bindApi() {
        bind(ComicsApi::class.java).toProvider(ComicsApiProvider::class.java)
            .providesSingletonInScope()
    }

    private fun bindRepos() {
        bind(ComicsRepository::class.java).toProvider(ComicsRepositoryProvider::class.java)
            .providesSingletonInScope()
    }

}
