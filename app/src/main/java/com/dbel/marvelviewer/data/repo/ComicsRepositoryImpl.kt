package com.dbel.marvelviewer.data.repo

import com.dbel.marvelviewer.data.models.comics.toDomainComicsList
import com.dbel.marvelviewer.data.sources.ComicsDataSource
import com.dbel.marvelviewer.domain.models.comics.ComicsLo
import com.dbel.marvelviewer.domain.repo.ComicsRepository
import io.reactivex.Flowable

class ComicsRepositoryImpl(
    private val remoteDataSource: ComicsDataSource,
    private val localDataSource: ComicsDataSource
) : ComicsRepository {
    override fun getComics(limit: Int, offset: Int): Flowable<List<ComicsLo>> {
        return remoteDataSource.loadComics(limit, offset)
            .map { it.toDomainComicsList() }
    }
}