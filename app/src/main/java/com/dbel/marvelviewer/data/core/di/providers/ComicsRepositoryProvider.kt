package com.dbel.marvelviewer.data.core.di.providers

import android.content.Context
import com.dbel.marvelviewer.data.core.network.api.marvel.ComicsApi
import com.dbel.marvelviewer.data.repo.ComicsRepositoryImpl
import com.dbel.marvelviewer.data.sources.local.ComicsLocalDataSource
import com.dbel.marvelviewer.data.sources.remote.ComicsRemoteDataSource
import com.dbel.marvelviewer.domain.repo.ComicsRepository
import javax.inject.Inject
import javax.inject.Provider

class ComicsRepositoryProvider @Inject constructor(
    private val context: Context,
    private val api: ComicsApi
) : Provider<ComicsRepository> {
    override fun get(): ComicsRepository {
        return ComicsRepositoryImpl(
            ComicsRemoteDataSource(context, api),
            ComicsLocalDataSource()
        )
    }
}